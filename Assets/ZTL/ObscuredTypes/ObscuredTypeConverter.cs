﻿using System;
using Newtonsoft.Json;

namespace ObscuredTypes
{
    public class ObscuredIntConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue((ObscuredInt)value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            ObscuredInt ret = Convert.ToInt32(reader.Value);
            return ret;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(int);
        }
    }
    
    public class ObscuredFloatConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue((ObscuredFloat)value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            ObscuredFloat ret = Convert.ToSingle(reader.Value);
            return ret;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(float);
        }
    }
}

namespace ES3Types
{
    using ObscuredTypes;

    [UnityEngine.Scripting.Preserve]
    public class ES3Type_ObscuredInt : ES3Type
    {
        public static ES3Type Instance = null;

        public ES3Type_ObscuredInt() : base(typeof(ObscuredInt))
        {
            isPrimitive = true;
            Instance = this;
        }

        public override void Write(object obj, ES3Writer writer)
        {
            int temp = (ObscuredInt)obj;
            writer.WritePrimitive(temp);
        }

        public override object Read<T>(ES3Reader reader)
        {
            return (ObscuredInt)reader.Read_int();
        }
    }

    public class ES3Type_ObscuredIntArray : ES3ArrayType
    {
        public static ES3Type Instance;

        public ES3Type_ObscuredIntArray() : base(typeof(ObscuredInt[]), ES3Type_ObscuredInt.Instance)
        {
            Instance = this;
        }
    }
    
    [UnityEngine.Scripting.Preserve]
    public class ES3Type_ObscuredFloat : ES3Type
    {
        public static ES3Type Instance = null;

        public ES3Type_ObscuredFloat() : base(typeof(ObscuredFloat))
        {
            isPrimitive = true;
            Instance = this;
        }

        public override void Write(object obj, ES3Writer writer)
        {
            float temp = (ObscuredFloat)obj;
            writer.WritePrimitive(temp);
        }

        public override object Read<T>(ES3Reader reader)
        {
            return (ObscuredFloat)reader.Read_float();
        }
    }

    public class ES3Type_ObscuredFloatArray : ES3ArrayType
    {
        public static ES3Type Instance;

        public ES3Type_ObscuredFloatArray() : base(typeof(ObscuredFloat[]), ES3Type_ObscuredFloat.Instance)
        {
            Instance = this;
        }
    }
}

namespace ZF
{
    using ZeroFormatter;
    using ZeroFormatter.Formatters;
    using ObscuredTypes;
    
    public class ObscuredIntFormatter<TTypeResolver> : Formatter<TTypeResolver, ObscuredInt>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, ObscuredInt value)
        {
            return Formatter<TTypeResolver, int>.Default.Serialize(ref bytes, offset, value);
        }

        public override ObscuredInt Deserialize(ref byte[] bytes, int offset, DirtyTracker tracker, out int byteSize)
        {
            return Formatter<TTypeResolver, int>.Default.Deserialize(ref bytes, offset, tracker, out byteSize);
        }
    }
    
    public class ObscuredFloatFormatter<TTypeResolver> : Formatter<TTypeResolver, ObscuredFloat>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, ObscuredFloat value)
        {
            return Formatter<TTypeResolver, float>.Default.Serialize(ref bytes, offset, value);
        }

        public override ObscuredFloat Deserialize(ref byte[] bytes, int offset, DirtyTracker tracker, out int byteSize)
        {
            return Formatter<TTypeResolver, float>.Default.Deserialize(ref bytes, offset, tracker, out byteSize);
        }
    }
}