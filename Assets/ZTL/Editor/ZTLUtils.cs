﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace ZTL.Editor
{
    public static class ZTLUtils
    {
        public static string ParseFileIdFromUri(string uriString)
        {
            const string SEGMENT_ON_PRE_FILE_ID = "d/";
            var uri = new Uri(uriString);

            bool isFindPreSegment = false;
            foreach (var segment in uri.Segments)
            {
                if (isFindPreSegment) 
                    return segment.Substring(0, segment.Length - 1); // 마지막 글자에 슬래시가 포함되어있음.

                if (segment.Equals(SEGMENT_ON_PRE_FILE_ID, StringComparison.OrdinalIgnoreCase)) 
                    isFindPreSegment = true;
            }
            throw new Exception($"file id를 찾을 수 없습니다! {uriString}");
        }

        public static string GetTimeElapsedString(DateTime from)
        {
            if (from == default) return "-";
            
            TimeSpan delta = DateTime.Now - from;
            return $"{from.ToString()} ({delta.Days}일 {delta.Hours}시간 {delta.Minutes}분 전)";
        }
    }

    public static class Debug
    {
        // ReSharper disable UnusedParameter.Global
        public static void Log(string str)
        {
#if ZTL_LOG
            UnityEngine.Debug.Log(str);
#endif
        }
        // ReSharper restore UnusedParameter.Global
        
        public static void LogWarning(string str)
        {
            UnityEngine.Debug.LogWarning(str);
        }
        
        public static void LogError(string str)
        {
            UnityEngine.Debug.LogError(str);
        }
    }
    
    [Serializable]
    public class SingletonScriptableObject<T> : ScriptableObject
        where T : ScriptableObject
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    string ztlSingletonAssetPath = Path.Combine($"{ZTLEditor.ZTL_EDITOR_PATH_FROM_ASSETS}",
                        $"{typeof(T).Name}.asset"); 
                    instance = (T) AssetDatabase.LoadAssetAtPath(ztlSingletonAssetPath, typeof(T));

                    if (instance == null)
                    {
                        instance = CreateInstance<T>();
                        AssetDatabase.CreateAsset(instance, ztlSingletonAssetPath);
                        AssetDatabase.Refresh();
                    }
                }
                return instance;
            }
        }
    }
}