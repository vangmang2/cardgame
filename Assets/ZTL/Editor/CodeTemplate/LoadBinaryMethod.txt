        private static string __encryptKey = @"@EncryptPassword";
        public static @ClassNameTable LoadBinary()
        {
            // load binary
            byte[] textBytes = UnityEngine.Resources.Load<UnityEngine.TextAsset>(@"@BinaryFilePath").bytes;
            if (__encryptKey.Length != 0)
            {
                var cryptoBytes = System.Text.Encoding.UTF8.GetBytes(__encryptKey);
                for (int i = 0; i < textBytes.Length; i++)
                    textBytes[i] ^= cryptoBytes[i & (cryptoBytes.Length - 1)];
            }
            var table = new @ClassNameTable(ZeroFormatterSerializer.Deserialize<Table<@DictionaryKeyType, @ClassNameRow>>(textBytes))
            {
                dataList = new List<@ClassNameRow>()
            };
            foreach (var pair in table.indexedData)
                table.dataList.Add(pair.Value);
            return table;
        }