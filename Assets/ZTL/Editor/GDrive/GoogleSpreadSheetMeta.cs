namespace ZTL.Editor.GDrive
{
    public class GoogleSpreadSheetMeta
    {
        public string fileName;
        public string fileId;
        public int gid;
        public string sheetName;
    }
}