using System;

namespace ZTL.Editor.GDrive
{
    [Serializable]
    public class GoogleSpreadSheetJson
    {
        [Serializable]
        public class Sheet
        {
            [Serializable]
            public class Property
            {
                [Serializable]
                public class GridProperty
                {
                    public int rowCount;
                    public int columnCount;
                }
                public int sheetId;
                public string title;
                public int index;
                public GridProperty gridProperties;
            }
            public Property properties;
        }
        public Sheet[] sheets;

        [Serializable]
        public class Property
        {
            public string title;
        }
        public Property properties;
        public string spreadsheetId;
        public string spreadsheetUrl;
    }
}