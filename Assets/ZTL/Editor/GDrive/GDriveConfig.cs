﻿using System;
using Sirenix.OdinInspector;

namespace ZTL.Editor.GDrive
{
    [Serializable]
    public class GDriveConfig : SingletonScriptableObject<GDriveConfig>
    {
        [InfoBox(
@"Google Drive에 접속할 수 있는 API에 대한 정보가 필요합니다. (Google API)
OAuth를 통해 인증하며 Google Sheet API, Google Drive API를 사용합니다.

Oauth를 통해 최초 로그인을 진행하면 ZTL Editor 경로에 cachedAuth.tmp 파일을 생성하여 발급받은 토큰 정보를 평문으로 저장합니다.

만약 다른 인증 정보를 사용한다면 cachedAuth.tmp 파일을 제거해야합니다."
            , InfoMessageType.Warning)]
        public string clientId;
        public string clientSecretKey;
        public string apiKey;
    }
}