﻿using System;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ZTL.Editor
{
    [Serializable]
    public class ZTLConfig : SingletonScriptableObject<ZTLConfig>
    {
        [Title("바이너리 암호화", "@GetPasswordSubDescription()")]
        [LabelText("암호키")] [InlineButton("MakeRandomPassword", "랜덤 암호 생성")]
        public string password = "";

        private void MakeRandomPassword()
        {
            var sb = new StringBuilder();
            const int RANDOM_PASSWORD_LENGTH = 16;
            for (int i = 0; i < RANDOM_PASSWORD_LENGTH; i++)
            {
                char charStart, charEnd;
                switch (UnityEngine.Random.Range(0, 3))
                {
                    case 0: // digit
                        charStart = '0'; charEnd = '9';
                        break;
                    case 1: // lower case
                        charStart = 'a'; charEnd = 'z';
                        break;
                    case 2: // upper case
                        charStart = 'A'; charEnd = 'Z';
                        break;
                    default:
                        throw new IndexOutOfRangeException();
                }
                sb.Append((char) UnityEngine.Random.Range(charStart, charEnd + 1));
            }
            password = sb.ToString();
        }

        private string GetPasswordSubDescription()
        {
            return string.IsNullOrEmpty(password) 
                ? "빈 암호는 바이너리를 평문으로 저장합니다."
                : string.Empty;
        }

        [Title("Sheet 로드 방법")] [HideLabel] [Indent]
        public SheetPreloader _preloader;

        [Header("Script 생성 경로")]
        [LabelText("Assets/")]
        [FolderPath(ParentFolder = "Assets/")]
        public string scriptPath = "Scripts/ZTLScripts";
    
        [Title("Binary 생성 경로", "Export시 생성되는 Script에 영향을 미치기 때문에 경로가 변경 될 시 다시 Export를 권장합니다.")]
        [LabelText("Assets/Resources/")]
        [FolderPath(ParentFolder = "Assets/Resources/")]
        [HideLabel] public string binaryPath = "TableBinary";
        
        [InlineButton("SetZfxParameterToDefaultValue", "기본값으로 초기화")]
        [LabelText("zfc.exe 매개변수"), PropertySpace(15, 15)] 
        public string zfcParameter = "-e";

        private void SetZfxParameterToDefaultValue()
        {
            zfcParameter = "-e";
        }
    
        [BoxGroup("Obscured Type")]
        [LabelText("Obscured Type 활성화")] public bool usingObscuredTypes;
    
        [BoxGroup("Obscured Type")]
        [ShowIf("usingObscuredTypes"), Required]
        [LabelText("Obscured Int 암호키")] public int obscuredIntCryptoKey = 284843;
    
        [BoxGroup("Obscured Type")]
        [ShowIf("usingObscuredTypes"), Required]
        [LabelText("Obscured Float 암호키")] public int obscuredFloatCryptoKey = 545662;

        public string BuildArguments()
        {
            StringBuilder sb = new StringBuilder();

            if (password.Length != 0)
                sb.Append($"-p {password} ");

            if (_preloader.GetSheetsRelativeDirectory().Length != 0)
                sb.Append($"-e {_preloader.GetSheetsRelativeDirectory()} ");

            if (scriptPath.Length != 0)
                sb.Append($"-s {scriptPath} ");

            if (binaryPath.Length != 0)
                sb.Append($"-b {binaryPath} ");

            if (zfcParameter.Length != 0)
                sb.Append($"-z {zfcParameter.Replace(" ", "#")} ");
        
            if (usingObscuredTypes)
                sb.Append($"-otype {obscuredIntCryptoKey}#{obscuredFloatCryptoKey} ");

            return sb.ToString();
        }

        public async Task Preprocess()
        {
            if (_preloader.importMode == SheetPreloader.ImportModeType.Local) return;
            if (_preloader.isAutoDownloadAtExport) 
                await _preloader.DownloadSheetsFromGoogleDriveAsync();
        }
    }
}