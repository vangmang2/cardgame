﻿using System;
using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace ZTL.Editor
{
    public class ZTLEditor : UnityEditor.Editor
    {
        private static string ZTLEditorWorkingDirectory => Path.Combine(Application.dataPath, "ZTL/Editor/");
        public static readonly string ZTL_EDITOR_PATH_FROM_ASSETS = "Assets/ZTL/Editor/";

        private static void ProcessExecute(string args)
        {
            Process p = new Process
            {
                StartInfo =
                {
                    FileName = Path.Combine(ZTLEditorWorkingDirectory, "ZFExportLite.exe"),
                    Arguments = args,
                    WorkingDirectory = ZTLEditorWorkingDirectory
                }
            };
        
            p.Start();
            p.WaitForExit();
            ShowLogByExitCode(p.ExitCode);
            
            Debug.Log($"[{nameof(ZTLEditor)}] ZFExportLite Process End. @{DateTime.Now}");
        }

        [MenuItem("Tools/ZTL/Quick Export _F8")]
        public static async void Export()
        {
            UnityEngine.Debug.Log($"[{nameof(ZTLEditor)}] Export Start @{DateTime.Now}");
            await ZTLConfig.Instance.Preprocess();
            Debug.Log($"[{nameof(ZTLEditor)}] Running ZFExportLite.. ZFExportLite.exe {ZTLConfig.Instance.BuildArguments()} @{DateTime.Now}");
            ProcessExecute(ZTLConfig.Instance.BuildArguments());
            AssetDatabase.Refresh();
        }

        [MenuItem("Tools/ZTL/Open Excel Path %F8", priority = 1)]
        public static void OpenExcelDirectory()
        {
            var path = sheetFileAbsoluteDirectory;

            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
        
            Process.Start(path);
        }

        public static string sheetFileAbsoluteDirectory =>
            Path.Combine(ZTLEditorWorkingDirectory,
                ZTLConfig.Instance._preloader.GetSheetsRelativeDirectory());

        private static void ShowLogByExitCode(int exitCode)
        {
            bool success = exitCode > 0;
            if (success)
            {
                UnityEngine.Debug.Log($"[{nameof(ZTLEditor)}] 파일 {exitCode} 개의 데이터를 처리했습니다.");
            }
            else
            {
                using (var fs = new FileStream(Path.Combine(ZTLEditorWorkingDirectory, "code-info.txt"), FileMode.Open))
                using (var rs = new StreamReader(fs))
                {
                    string line;
                    while ((line = rs.ReadLine()) != null)
                    {
                        string[] texts = line.Split('|');
                        int code = int.Parse(texts[0]);
                    
                        if (code == exitCode)
                        {
                            string codeDesc = texts[1];
                            Debug.LogError($"[{nameof(ZTLEditor)}] ({exitCode}): {codeDesc}");
                            return;
                        }
                    }
                }
            
                Debug.LogError($"[{nameof(ZTLEditor)}] 알 수 없는 에러가 발생했습니다. ({exitCode})");
            }
        }
    }
}