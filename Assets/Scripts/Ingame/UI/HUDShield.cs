﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDShield : MonoBehaviour
{
    [SerializeField] Image imgShield;
    [SerializeField] Text txtShield;

    public void SetShield(int shield, int maxShield)
    {
        imgShield.fillAmount = (float)shield / maxShield;
        txtShield.text = $"{shield}/{maxShield}";
    }
}
