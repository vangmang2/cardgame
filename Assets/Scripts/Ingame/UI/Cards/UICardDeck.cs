﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using System.Linq;

public class UICardDeck : MonoBehaviour
{
    public readonly int maxLimitedCardCount = 25;
    public readonly Queue<UIItemCard> cardQueue = new Queue<UIItemCard>();

    [SerializeField] bool visible;
    [SerializeField] UIItemCard cardPrefab;

    /// <summary> 카드 보이기, 삭제 등 연출에 쓰이는 리스트 </summary>
    public readonly List<UIItemCard> cardList = new List<UIItemCard>();
    int registeredCount = 0;

    readonly Vector2 startPos = new Vector2(35f, -150f);
    readonly float space = 50f;

    public void RegisterCard(Action<UIItemCard, bool> callback_setInfo)
    {
        UIItemCard itemCard = null;
        registeredCount++;

        if (cardQueue.Count < maxLimitedCardCount)
            itemCard = Instantiate(cardPrefab, transform);
        else
            itemCard = cardQueue.Dequeue();

        var pos = startPos;
        pos.x += registeredCount * space;
        itemCard.SetOriginPos(pos);
        itemCard.getRtBody.anchoredPosition = pos;
        itemCard.getRtBody.SetSiblingIndex(registeredCount - 1);

        cardQueue.Enqueue(itemCard);
        cardList.Add(itemCard);
        callback_setInfo?.Invoke(itemCard, visible);
    }

    // 카드가 쿨타임이면 나타나지 않는다.
    Coroutine coroutine;
    public void ShowCards(Action callback)
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoShowCards(callback));
    }

    public void ResetCards()
    {
        cardList.Clear();
    }

    public void ClearCards()
    {
        registeredCount = 0;
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoClearCards());
    }

    private IEnumerator CoShowCards(Action callback)
    {
        float max = 3f;
        float t = max / cardList.Count;

        foreach (var card in cardList)
        {
            // 카드가 함선에서 카드 덱구역으로 슝슝 날아오는 연출
            card.gameObject.SetActive(true);
            card.getRtBody.localScale = Vector3.zero;
            card.getRtBody.anchoredPosition = new Vector2(1062.3f, 59.8f);
            card.getRtBody.localRotation = Quaternion.Euler(0f, 0f, 58.451f);

            card.getRtBody.DOAnchorPos(card.originPos, t).SetEase(Ease.InOutSine);
            card.getRtBody.DOLocalRotate(Vector3.zero, t).SetEase(Ease.InOutSine);
            card.getRtBody.DOScale(Vector3.one, t).SetEase(Ease.InOutSine);
            card.PlaySfx_CardSwap();
            yield return new WaitForSeconds(t * 0.25f);
        }
        yield return new WaitForSeconds(t);
        callback?.Invoke();
        SetCardRaycast(true);
    }

    // 카드가 왼쪽으로 사라지는 연출
    private IEnumerator CoClearCards()
    {
        //List list = new System.Collections.Generic.List();
        SetCardRaycast(false);
        float max = 3;
        float t = max / cardList.Count;
        for(int i = cardList.Count - 1; i >= 0; i--)
        {
            var card = cardList[i];
            card.getRtBody.DOAnchorPosX(-1280f, t).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                card.gameObject.SetActive(false);
            });
            yield return new WaitForSeconds(t * 0.25f);
        }
        yield return new WaitForSeconds(t);
        cardList.Clear();
    }

    private void SetCardRaycast(bool enable)
    {
        cardList.ForEach(card =>
        {
            card.SetRaycast(enable);
        });
    }
}
