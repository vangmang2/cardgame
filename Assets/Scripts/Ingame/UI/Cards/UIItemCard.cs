﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class UIItemCard : MonoBehaviour, IPointerDownHandler, IEndDragHandler
{
    [SerializeField] RectTransform rtBody;
    [SerializeField] Text txtName, txtDescription, txtPowerUsage, txtCooldown;
    [SerializeField] Image imgType, imgIcon, imgBg;
    [SerializeField] AudioSource audioCardSwap;

    Action<UIItemCard> action_pointerDown;
    Func<bool> action_useCard;
    Func<UIItemCard, bool> action_endDrag;

    public RectTransform getRtBody => rtBody;
    public int cardIndex { get; private set; }
    public Vector2 originPos { get; private set; }
    public ModuleType moduleType { get; private set; }

    public int originPowerUsage;

    public string cardName => txtName.text;

    public void PlaySfx_CardSwap()
    {
        audioCardSwap.Play();
    }

    public void ResetPowerUsageText()
    {
        SetPowerUsageText(originPowerUsage);
    }

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
    }

    public UIItemCard SetOriginPos(Vector2 pos)
    {
        originPos = pos;
        return this;
    }

    public UIItemCard SetCardIndex(int index)
    {
        cardIndex = index;
        return this;
    }

    public UIItemCard SetCardCooldown(int cooldown)
    {
        txtCooldown.text = $"쿨타임: {cooldown}";
        return this;
    }

    public UIItemCard SetInteraction(bool enable)
    {
        imgType.gameObject.SetActive(enable);
        imgIcon.gameObject.SetActive(enable);
        txtName.gameObject.SetActive(enable);
        txtDescription.gameObject.SetActive(enable);
        txtPowerUsage.gameObject.SetActive(enable);
        imgBg.raycastTarget = enable;
        return this;
    }

    public UIItemCard SetActionUseCard(Func<bool> callback)
    {
        action_useCard = callback;
        return this;
    }

    public UIItemCard SetRaycast(bool enable)
    {
        imgBg.raycastTarget = enable;
        return this;
    }

    public UIItemCard SetNameText(string name)
    {
        txtName.text = name;
        return this;
    }

    public UIItemCard SetDescriptionText(string desc)
    {
        txtDescription.text = desc;
        return this;
    }

    public UIItemCard SetPowerUsageText(int power)
    {
        txtPowerUsage.text = power.ToString();
        return this;
    }

    public UIItemCard SetType(ModuleType type)
    {
        moduleType = type;
        return this;
    }

    public UIItemCard SetOriginPowerUsageText(int originPowerUsage)
    {
        this.originPowerUsage = originPowerUsage;
        return this;
    }

    public UIItemCard SetTypeImage(ModuleType type)
    {
        imgType.sprite = AssetManager.instance.GetTypeSprite(type);
        return this;
    }

    public UIItemCard SetIconImage(Sprite sprite)
    {
        imgIcon.sprite = sprite;
        return this;
    }

    public UIItemCard SetActionPointerDown(Action<UIItemCard> callback)
    {
        action_pointerDown = callback;
        return this;
    }

    public UIItemCard SetActionEndDrag(Func<UIItemCard, bool> callback)
    {
        action_endDrag = callback;
        return this;
    }

    // 카드가 클릭된 동안 다른 카드는 선택 불가
    public void OnPointerDown(PointerEventData eventData)
    {
        action_pointerDown?.Invoke(this);
    }

    // 혹은 클릭이 끝날 떄 까지
    public void OnEndDrag(PointerEventData eventData)
    {
        action_endDrag?.Invoke(this);
    }

    public bool UseCard()
    {
        return action_useCard();
    }
}
