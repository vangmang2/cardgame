﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemExitBtn : MonoBehaviour
{
    [SerializeField] Image imgBtn;

    public void SetDim(bool enable)
    {
        imgBtn.raycastTarget = !enable;
        imgBtn.color = enable ? Color.gray : Color.white;
    }
}
