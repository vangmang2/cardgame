﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using ZF;
using System;

public class ModuleBase : MonoBehaviour
{
    [SerializeField] ModuleType type;
    [SerializeField] protected ShipBase parentShip;
    [SerializeField] protected UIItemModule itemModule;

    public bool isMyModule; 
    public int maxHitpoints { get; private set; }
    public Vector2 modulePosition => itemModule.getRtBody.position;
    [SerializeField] protected int m_hitpoints;
    protected bool isCardAboutToUse;

    public void EnableCardUsable()
    {
        isCardAboutToUse = true;
    }

    public int hitpoints => m_hitpoints;
    public void SetMaxHitpoints(int hitpoints)
    {
        maxHitpoints = hitpoints;
        m_hitpoints = hitpoints;
        itemModule.SetHitpoints(m_hitpoints, maxHitpoints);
    }

    public void GainHitpoints(int hitpoints)
    {
        m_hitpoints += hitpoints;
        m_hitpoints = Mathf.Min(m_hitpoints, maxHitpoints);
        itemModule.SetHitpoints(m_hitpoints, maxHitpoints);
    }

    public void SetHitpoints(int hitpoints)
    {
        m_hitpoints = hitpoints;
        m_hitpoints = Mathf.Min(m_hitpoints, maxHitpoints);
        itemModule.SetHitpoints(m_hitpoints, maxHitpoints);
    }


    Coroutine coroutine;
    // 카드를 가운데로 이동시켜 보여준다.
    public virtual void SetActionPointerDown(UIItemCard itemCard)
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoSetPointerDown(itemCard));
    }

    private IEnumerator CoSetPointerDown(UIItemCard itemCard)
    {
        // Temp 
        itemCard.SetRaycast(false);
        itemCard.getRtBody.DOKill();
        itemCard.getRtBody.DOAnchorPos(new Vector2(640f, 220f), 0.3f).SetEase(Ease.InOutSine);
        itemCard.getRtBody.DOScale(new Vector2(1.3f, 1.3f), 0.3f).SetEase(Ease.InOutSine);

        bool isCardAboutToUse = false;
        while (Input.GetMouseButton(0))
        {
            var mousePos = Input.mousePosition;
            this.isCardAboutToUse = mousePos.y > 152f;
            if (mousePos.y > 152f && !isCardAboutToUse)
            {
                itemCard.getRtBody.DOKill();
                itemCard.getRtBody.DOAnchorPos(new Vector2(1051f, 126f), 0.2f).SetEase(Ease.InOutSine);
                itemCard.getRtBody.DOLocalRotate(new Vector3(0f, 0f, 69.08f), 0.2f).SetEase(Ease.InOutSine);
                itemCard.getRtBody.DOScale(new Vector2(0.5f, 0.5f), 0.2f).SetEase(Ease.InOutSine);
                isCardAboutToUse = true;
            }
            else if(mousePos.y <= 152f && isCardAboutToUse)
            {
                itemCard.getRtBody.DOKill();
                itemCard.getRtBody.DOAnchorPos(new Vector2(640f, 220f), 0.3f).SetEase(Ease.InOutSine);
                itemCard.getRtBody.DOLocalRotate(Vector3.zero, 0.3f).SetEase(Ease.InOutSine);
                itemCard.getRtBody.DOScale(new Vector2(1.3f, 1.3f), 0.3f).SetEase(Ease.InOutSine);
                isCardAboutToUse = false;
            }
            yield return null;
        }

        yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
        if(!EndDrag(itemCard))
        {
            itemCard.getRtBody.DOKill();
            itemCard.getRtBody.DOAnchorPos(itemCard.originPos, 0.3f).SetEase(Ease.InOutSine);
            itemCard.getRtBody.DOLocalRotate(Vector3.zero, 0.3f).SetEase(Ease.InOutSine);
            itemCard.getRtBody.DOScale(Vector2.one, 0.3f).SetEase(Ease.InOutSine).OnComplete(() => itemCard.SetRaycast(true));
        }
    }

    /// <summary>
    /// 내 턴이 되었을 때 카드 등록됨
    /// </summary>
    public virtual void RegisterCard() { }

    public virtual bool EndDrag(UIItemCard itemCard)
    {
        return UIItemModule.selectedModule != null;
    }

    public virtual void TakeDamageByWeapon(int damage)
    {
        m_hitpoints -= damage;
        m_hitpoints = Mathf.Max(m_hitpoints, 0);

        if (type == ModuleType.Hull)
            parentShip.TakeDamage(damage);
        itemModule.SetHitpoints(m_hitpoints, maxHitpoints);
    }

    public void TakeDamage(int amount)
    {
        if (parentShip.currentShield > 0)
        {
            int overTakenDamage = parentShip.ReduceShield(amount);
            if(overTakenDamage < 0)
                TakeDamageByWeapon(Mathf.Abs(overTakenDamage));
        }
        else
        {
            TakeDamageByWeapon(amount);
        }
    }

    public virtual void UseCard(UIItemCard itemCard, Action callback)
    {
        itemCard.getRtBody.DOScale(Vector2.zero, 0.3f).SetEase(Ease.InSine).OnComplete(() =>
        {
            callback?.Invoke();
            if(type != ModuleType.Hull)
                parentShip.powerBatteryCount--;
            if(parentShip.powerBatteryCount == 0)
              parentShip.EnablePowerBattery(false);
        });
    }
}
