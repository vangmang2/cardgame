﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class UIItemModule : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public static ModuleBase emenyTempModule;
    public static ModuleBase selectedModule;
    
    [SerializeField] RectTransform rtBody;
    [SerializeField] Text txtModuleType, txtHitpoints;
    [SerializeField] Image imgHitpoints, imgBg;
    [SerializeField] GameObject goIndicator;

    [SerializeField] ParticleSystem particleOverheat;
    [SerializeField] Image imgShieldBattery;

    public RectTransform getRtBody => rtBody;

    ModuleBase parentModule;
    public UIItemModule SetParentModule(ModuleBase parentModule)
    {
        this.parentModule = parentModule;
        return this;
    }
    public UIItemModule SetModuleTypeText(ModuleType type)
    {
        switch (type)
        {
            case ModuleType.Weapon: 
                txtModuleType.text = "무기";
                break;
            case ModuleType.Shield:
                txtModuleType.text = "쉴드";
                break;
            case ModuleType.Engine:
                txtModuleType.text = "엔진";
                break;
            case ModuleType.Hull:
                txtModuleType.text = "선체";
                break;
        }
        return this;
    }


    public void SetHitpoints(int currentHP, int maxHP)
    {
        txtHitpoints.text = $"{currentHP}/{maxHP}";
        imgHitpoints.fillAmount = (float)currentHP / maxHP;
    }

    public void PlayOverheatParticle()
    {
        particleOverheat.Play();
    }

    public void StopOverheatParticle()
    {
        particleOverheat.Stop();
    }

    public void PlayShieldBatteryEffect()
    {
        var color1 = new Color32(0x3D, 0x6A, 0xFF, 0xFF);
        var color2 = new Color32(0x3D, 0xF0, 0xFF, 0xFF);
        bool changeColor = true;
        imgShieldBattery.DOKill();
        imgShieldBattery.DOColor(changeColor ? color1 : color2, 0.5f).OnComplete(() =>
        {
            changeColor = !changeColor;
        }).SetLoops(-1, LoopType.Yoyo);
    }

    public void StopShieldBatteryEffect()
    {
        imgShieldBattery.DOKill();
        imgShieldBattery.DOColor(new Color32(0x3D, 0x6A, 0xFF, 0x00), 0.5f);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        selectedModule = parentModule;
        goIndicator.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        selectedModule = null;
        goIndicator.SetActive(false);
    }
}
