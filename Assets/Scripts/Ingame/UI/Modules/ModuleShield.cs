﻿using System.Collections;
using System.Collections.Generic;
using ZF;
using UnityEngine;

public class ModuleShield : ModuleBase
{
    [SerializeField] UICardDeck cardDeck;
    
    public void SetInfo()
    {
        itemModule.SetModuleTypeText(ModuleType.Shield)
                  .SetParentModule(this);
    }

    public override void RegisterCard()
    {
        if (hitpoints <= 0)
            return;

        for (int i = 0; i < DataManager.shieldData.dataList.Count; i++)
        {
            var shieldData = DataManager.GetShieldData(i);
            if (GameManager.instance.CooldownChecker(shieldData.cooldown))
            {
                cardDeck.RegisterCard((card, visible) =>
                {
                    card.SetCardIndex(i)
                        .SetCardCooldown(shieldData.cooldown)
                        .SetNameText(shieldData.name)
                        .SetDescriptionText(GetDescription(shieldData))
                        .SetOriginPowerUsageText(shieldData.powerUsage)
                        .SetType(ModuleType.Shield)
                        .SetPowerUsageText(GetPowerUsage(shieldData))
                        .SetTypeImage(ModuleType.Shield)
                        .SetIconImage(AssetManager.instance.GetShieldSprite(i))
                        .SetActionPointerDown(SetActionPointerDown)
                        .SetActionEndDrag(EndDrag)
                        .SetInteraction(visible)
                        .SetActionUseCard(() =>
                        {
                            switch (shieldData.id)
                            {
                                case 1:
                                    return RecoveryShield(card, shieldData);
                                case 2:
                                    return ActivateShieldBattery(card, shieldData);
                                case 3:
                                    return DrainShield(card, shieldData);
                                default:
                                    return false;
                            }
                        });
                });
            }
        }
    }

    public void PlayShieldBatteryEffect()
    {
        itemModule.PlayShieldBatteryEffect();
    }

    public void StopShieldBatteryEffect()
    {
        itemModule.StopShieldBatteryEffect();
    }

    private int GetPowerUsage(ShieldDataRow shieldData)
    {
        var power = parentShip.isShieldBatteryEnable ? shieldData.powerUsage - 1 : shieldData.powerUsage;
        if (parentShip.isPowerBatteryEnable)
            power = 1;
        return power;
    }

    private string GetDescription(ShieldDataRow shieldData)
    {
        string format = string.Empty;
        switch (shieldData.id)
        {
            case 1:
                format = string.Format(shieldData.description, 4);
                return format;
            case 2:
                format = string.Format(shieldData.description, 1);
                return format;
            case 3:
                format = shieldData.description;
                return format;
            default:
                return "";
        }

    }

    public override bool EndDrag(UIItemCard itemCard)
    {
        return itemCard.UseCard();
    }

    private bool RecoveryShield(UIItemCard itemCard, ShieldDataRow shieldData)
    {
        var powerUsage = parentShip.isPowerBatteryEnable ? 1 : shieldData.powerUsage;
        if (parentShip.isShieldBatteryEnable)
            powerUsage--;

        bool canUse = parentShip.power - shieldData.powerUsage >= 0;
        canUse = parentShip.power - powerUsage >= 0;
        if (canUse)
        {
            if (isCardAboutToUse)
            {
                if (parentShip.shieldHitpoints >= 10)
                    return false;
                UseCard(itemCard, () =>
                {
                    parentShip.RecoveryShield(4);
                    parentShip.UsePower(powerUsage);
                });
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    private bool ActivateShieldBattery(UIItemCard itemCard, ShieldDataRow shieldData)
    {
        var powerUsage = parentShip.isPowerBatteryEnable ? 1 : shieldData.powerUsage;
        bool canUse = parentShip.power - powerUsage >= 0;
        if (canUse)
        {
            if (isCardAboutToUse)
            {
                UseCard(itemCard, () =>
                {
                    parentShip.ActivateShieldBattery(true);
                    parentShip.UsePower(powerUsage);
                });
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    private bool DrainShield(UIItemCard itemCard, ShieldDataRow shieldData)
    {
        var powerUsage = parentShip.isPowerBatteryEnable ? 1 : shieldData.powerUsage;
        bool canUse = parentShip.power - powerUsage >= 0;
        if (canUse)
        {
            if (isCardAboutToUse)
            {
                if (parentShip.shieldHitpoints >= 10)
                    return false;
                UseCard(itemCard, () =>
                {
                    UseCard(itemCard, () =>
                    {
                        parentShip.RecoveryShield(parentShip.getTargetShip.shieldHitpoints / 2);
                        parentShip.UsePower(powerUsage);
                    });
                });
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }
}
