﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using ZF;

public class ModuleWeapon : ModuleBase
{
    [SerializeField] UICardDeck cardDeck;
    [SerializeField] ProjectileController projectilController;
    [SerializeField] Transform[] launchableTransform;
    [SerializeField] MissileController missileController;

    
    public void SetInfo()
    {
        itemModule.SetModuleTypeText(ModuleType.Weapon)
                  .SetParentModule(this);
    }

    public override void RegisterCard()
    {
        if (hitpoints <= 0)
            return;
        for (int i = 0; i < DataManager.weaponData.dataList.Count; i++)
        { 
            // 무기 카드는 두 번 나옴
            for (int n = 0; n < 2; n++)
            {
                var weaponData = DataManager.GetWeaponData(i);
                if (GameManager.instance.CooldownChecker(weaponData.cooldown))
                {
                    cardDeck.RegisterCard((card, visible) =>
                    {
                        card.SetCardIndex(i)
                            .SetCardCooldown(weaponData.cooldown)
                            .SetNameText(weaponData.name)
                            .SetDescriptionText(GetDescription(weaponData))
                            .SetOriginPowerUsageText(weaponData.powerUsage)
                            .SetType(ModuleType.Weapon)
                            .SetPowerUsageText(GetPowerUsage(weaponData))
                            .SetTypeImage(ModuleType.Weapon)
                            .SetIconImage(AssetManager.instance.GetWeaponSprite(i))
                            .SetActionPointerDown(SetActionPointerDown)
                            .SetActionEndDrag(EndDrag)
                            .SetInteraction(visible)
                            .SetActionUseCard(() =>
                            {
                                switch (weaponData.id)
                                {
                                    case 1:
                                        return PlasmaProjectile(card, weaponData);
                                    case 2:
                                        return UraniumProjectile(card, weaponData);
                                    case 3:
                                        return LaunchMissile(card, weaponData);
                                    default:
                                        return false;
                                }
                            });
                    });
                }
            }
        }
    }
    public void PlayOverheatParticle()
    {
        itemModule.PlayOverheatParticle();
    }

    public void StopOverheatParticle()
    {
        itemModule.StopOverheatParticle();
    }

    private int GetPowerUsage(WeaponDataRow weaponData)
    {
        int power = parentShip.isOverHeat ? weaponData.powerUsage * 2 : weaponData.powerUsage;
        if (parentShip.isPowerBatteryEnable)
            power = 1;
        return power;
    }

    private string GetDescription(WeaponDataRow weaponData)
    {
        string format = string.Empty;
        switch (weaponData.id)
        {
            case 1:
                format = string.Format(weaponData.description, parentShip.isOverHeat ? weaponData.damage * 2 : weaponData.damage);
                return format;
            case 2:
                format = string.Format(weaponData.description, parentShip.isOverHeat ? weaponData.damage * 2 : weaponData.damage);
                return format;
            case 3:
                format = string.Format(weaponData.description, parentShip.isOverHeat ? weaponData.damage * 2 : weaponData.damage);
                return format;
            default:
                return "";
        }
    }

    public override bool EndDrag(UIItemCard itemCard)
    {
        return itemCard.UseCard();
    }

    // 플라즈마 포탄
    private bool PlasmaProjectile(UIItemCard itemCard, WeaponDataRow weaponData)
    {
        var selectedModule = UIItemModule.selectedModule;
        if (selectedModule != null)
        {
            // 공격 대상이 내 모듈이면 아무 것도 하지 않는다.
            if (parentShip.isPlayer && selectedModule.isMyModule)
                return false;
            else if (selectedModule.hitpoints <= 0)
                return false;
            else
            {
                var powerUsage = parentShip.isOverHeat ? weaponData.powerUsage * 2 : weaponData.powerUsage;
                powerUsage = parentShip.isPowerBatteryEnable ? 1 : powerUsage;
                var damage = parentShip.isOverHeat ? weaponData.damage * 2 : weaponData.damage;
                bool canUse = parentShip.power - powerUsage >= 0;

                if (canUse)
                {
                    UseCard(itemCard, () =>
                    {
                        parentShip.UsePower(powerUsage);

                        var startPos = launchableTransform[Random.Range(0, launchableTransform.Length)].position;
                        var endPos = selectedModule.modulePosition;
                        var sprite = AssetManager.instance.GetProjectileSprite(weaponData.id - 1);

                        projectilController.FireProjectile(startPos, endPos, sprite, new Color32(0x9F, 0x27, 0x20, 0xFF), new Color32(0xC3, 0x00, 0x99, 0xFF),
                                () =>
                                {
                                    selectedModule.TakeDamage(damage);
                                    CameraShaker.instance.ShakeCamera();
                                });
                    });
                }
                else
                    return false;
                
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    // 우라늄 포탄
    private bool UraniumProjectile(UIItemCard itemCard, WeaponDataRow weaponData)
    {
        var selectedModule = UIItemModule.selectedModule;
        if (selectedModule != null)
        {
            // 공격 대상이 내 모듈이면 아무 것도 하지 않는다.
            if (parentShip.isPlayer && selectedModule.isMyModule)
                return false;
            else if (selectedModule.hitpoints <= 0)
                return false;
            else
            {
                var powerUsage = parentShip.isOverHeat ? weaponData.powerUsage * 2 : weaponData.powerUsage;
                powerUsage = parentShip.isPowerBatteryEnable ? 1 : powerUsage;
                var damage = parentShip.isOverHeat ? weaponData.damage * 2 : weaponData.damage;
                bool canUse = parentShip.power - powerUsage >= 0;

                if (canUse)
                {
                    UseCard(itemCard, () =>
                    {
                        parentShip.UsePower(powerUsage);

                        var startPos = launchableTransform[Random.Range(0, launchableTransform.Length)].position;
                        var endPos = selectedModule.modulePosition;
                        var sprite = AssetManager.instance.GetProjectileSprite(weaponData.id - 1);

                        projectilController.FireProjectile(startPos, endPos, sprite, new Color32(0x3B, 0xA1, 0x00, 0xFF), new Color32(0x00, 0xC3, 0xBA, 0xFF),
                                () =>
                                {
                                    selectedModule.TakeDamage(damage);
                                    CameraShaker.instance.ShakeCamera();
                                });
                    });
                }
                else
                    return false;

            }
            return true;
        }
        else
        {
            return false;
        }
    }

    // 미사일 발사
    private bool LaunchMissile(UIItemCard itemCard, WeaponDataRow weaponData)
    {
        var powerUsage = parentShip.isOverHeat ? weaponData.powerUsage * 2 : weaponData.powerUsage;
        powerUsage = parentShip.isPowerBatteryEnable ? 1 : powerUsage;
        var damage = parentShip.isOverHeat ? weaponData.damage * 2 : weaponData.damage;
        bool canUse = parentShip.power - powerUsage >= 0;

        if (canUse)
        {
            if (isCardAboutToUse)
            {
                UseCard(itemCard, () =>
                {
                    parentShip.UsePower(powerUsage);
                    var list = parentShip.getTargetShip.moduleList.Where(module => module.hitpoints > 0).ToList();
                    var rand = new System.Random();
                    int index1 = rand.Next(0, list.Count());
                    var randomModule1 = list[index1];
                    int index2 = rand.Next(0, list.Count());
                    var randomModule2 = list[index2];

                    var launchPos = launchableTransform[1].position;
                    var eulerAngle = parentShip.transform.rotation.eulerAngles;

                    missileController.LaunchMissile(launchPos, randomModule1.modulePosition, eulerAngle.z + 90f,
                        () =>
                        {
                            randomModule1.TakeDamage(damage);
                            CameraShaker.instance.ShakeCamera();
                        });
                    StartCoroutine(CoDelay(() =>
                    {
                        missileController.LaunchMissile(launchPos, randomModule2.modulePosition, eulerAngle.z - 90f, () =>
                        {
                            randomModule2.TakeDamage(damage);
                            CameraShaker.instance.ShakeCamera();
                        });
                    }));
                });
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    private IEnumerator CoDelay(System.Action callback)
    {
        yield return new WaitForSeconds(0.25f);
        callback();
    }
}
