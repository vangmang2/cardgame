﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZF;

public class ModuleEngine : ModuleBase
{
    [SerializeField] UICardDeck cardDeck;
    
    public void SetInfo()
    {
        itemModule.SetModuleTypeText(ModuleType.Engine)
                  .SetParentModule(this);

    }

    public override void RegisterCard()
    {
        if (hitpoints <= 0)
            return;

        for (int i = 0; i < DataManager.engineData.dataList.Count; i++)
        {
            var engineData = DataManager.GetEngineData(i);
            if (GameManager.instance.CooldownChecker(engineData.cooldown))
            {
                cardDeck.RegisterCard((card, visible) =>
                {
                    card.SetCardIndex(i)
                        .SetCardCooldown(engineData.cooldown)
                        .SetNameText(engineData.name)
                        .SetDescriptionText(GetDescription(engineData))
                        .SetOriginPowerUsageText(engineData.powerUsage)
                        .SetType(ModuleType.Engine)
                        .SetPowerUsageText(parentShip.isPowerBatteryEnable ? 1 : engineData.powerUsage)
                        .SetTypeImage(ModuleType.Engine)
                        .SetIconImage(AssetManager.instance.GetEngineSprite(i))
                        .SetActionPointerDown(SetActionPointerDown)
                        .SetActionEndDrag(EndDrag)
                        .SetInteraction(visible)
                        .SetActionUseCard(() =>
                        {
                            switch (engineData.id)
                            {
                                case 1:
                                    return SupplyPower(card, engineData);
                                case 2:
                                    return Overload(card, engineData);
                                case 3:
                                    return ActivatePowerBattery(card, engineData);
                                default:
                                    return false;
                            }

                        });
                });
            }
        }
    }

    private string GetDescription(EngineDataRow engineData)
    {
        string format = string.Empty;
        switch (engineData.id)
        {
            case 1:
                format = string.Format(engineData.description, 2);
                return format;
            case 2:
                format = string.Format(engineData.description, 5);
                return format;
            case 3:
                format = engineData.description;
                return format;
            default:
                return "";
        }
    }

    public override bool EndDrag(UIItemCard itemCard)
    {
        return itemCard.UseCard();
    }

    private bool SupplyPower(UIItemCard itemCard, EngineDataRow engineData)
    {
        var powerUsage = parentShip.isPowerBatteryEnable ? 1 : engineData.powerUsage;
        bool canUse = parentShip.power - powerUsage >= 0;
        if (!canUse)
            return false;
        if (parentShip.power >= parentShip.maxPower)
            return false;
        else if (!isCardAboutToUse)
            return false;
        else
        {
            UseCard(itemCard, () =>
            {
                parentShip.UsePower(powerUsage);
                parentShip.RecoveryPower(2);
            });
            return true;
        }
    }

    private bool Overload(UIItemCard itemCard, EngineDataRow engineData)
    {
        var powerUsage = parentShip.isPowerBatteryEnable ? 1 : engineData.powerUsage;
        bool canUse = parentShip.power - engineData.powerUsage >= 0;
        if (!canUse)
            return false;
        if (parentShip.power >= parentShip.maxPower)
            return false;
        else if (!isCardAboutToUse)
            return false;
        else
        {
            UseCard(itemCard, () =>
            {
                parentShip.SetOverload(true);
                parentShip.UsePower(powerUsage);
                parentShip.RecoveryPower(5);
            });
            return true;
        }
    }

    private bool ActivatePowerBattery(UIItemCard itemCard, EngineDataRow engineData)
    {
        bool canUse = parentShip.power - engineData.powerUsage >= 0;
        if (!canUse)
            return false;
        if (!isCardAboutToUse)
            return false;
        else
        {
            UseCard(itemCard, () =>
            {
                parentShip.EnablePowerBattery(true);
                parentShip.UsePower(engineData.powerUsage);
            });
            return true;
        }
    }
}
