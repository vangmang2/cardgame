﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZF;

public class ModuleHull : ModuleBase
{
    [SerializeField] UICardDeck cardDeck;
    [SerializeField] Transform doomsdayLauncherTransform;
    [SerializeField] DoomsdayController doomsdayController;

    
    public void SetInfo()
    {
        itemModule.SetModuleTypeText(ModuleType.Hull)
                  .SetParentModule(this);
    }

    public override void RegisterCard()
    {
        for (int i = 0; i < DataManager.hullData.dataList.Count; i++)
        {
            var hullData = DataManager.GetHullData(i);
            if (GameManager.instance.CooldownChecker(hullData.cooldown))
            {
                cardDeck.RegisterCard((card, visible) =>
                {
                    card.SetCardIndex(i)
                        .SetCardCooldown(hullData.cooldown)
                        .SetNameText(hullData.name)
                        .SetDescriptionText(GetDescription(hullData))
                        .SetOriginPowerUsageText(hullData.powerUsage)
                        .SetType(ModuleType.Hull)
                        .SetPowerUsageText(parentShip.isEngineDown ? 1 : hullData.powerUsage)
                        .SetTypeImage(ModuleType.Hull)
                        .SetIconImage(AssetManager.instance.GetHullSprite(i))
                        .SetActionPointerDown(SetActionPointerDown)
                        .SetActionEndDrag(EndDrag)
                        .SetInteraction(visible)
                        .SetActionUseCard(() =>
                        {
                            switch (hullData.id)
                            {
                                case 1:
                                    return RepairModule(card, hullData);
                                case 2:
                                    return RebuildModule(card, hullData);
                                case 3:
                                    return Overheat(card, hullData);
                                case 4:
                                    return OperateDoomsday(card, hullData);
                                default:
                                    return false;
                            }
                        });
                });
            }
        }
    }

    private string GetDescription(HullDataRow hullData)
    {
        string format = string.Empty;
        switch(hullData.id)
        {
            case 1:
                format = string.Format(hullData.description, 3);
                return format;
            case 2:
                format = hullData.description;
                return format;
            case 3:
                format = hullData.description;
                return format;
            case 4:
                format = string.Format(hullData.description, 20);
                return format;
            default:
                return "";
        }
    }

    public override bool EndDrag(UIItemCard itemCard)
    {
        return itemCard.UseCard();
    }


    private bool RepairModule(UIItemCard itemCard, HullDataRow hullData)
    {
        var powerUsage = parentShip.isEngineDown ? 1 : hullData.powerUsage;
        bool canUse = parentShip.power - powerUsage >= 0;
        if (!canUse)
            return false;
        if (UIItemModule.selectedModule == null)
            return false;
        else
        {
            var selectedModule = UIItemModule.selectedModule;
            if (selectedModule.hitpoints == 0) // 내구도 0 이면 수리 불가능
                return false;

            if (selectedModule.isMyModule &&
                selectedModule.hitpoints < selectedModule.maxHitpoints)
            {
                UseCard(itemCard, () =>
                {
                    selectedModule.GainHitpoints(2);
                    parentShip.UsePower(powerUsage);
                });
                return true;
            }
            else
                return false;
        }
    }

    private bool RebuildModule(UIItemCard itemCard, HullDataRow hullData)
    {
        var powerUsage = parentShip.isEngineDown ? 1 : hullData.powerUsage;
        bool canUse = parentShip.power - powerUsage >= 0;
        if (!canUse)
            return false;
        else
        {
            var selectedModule = isMyModule ? UIItemModule.selectedModule : UIItemModule.emenyTempModule;
            if (selectedModule == null)
                return false;
            if (selectedModule.hitpoints > 0) // 내구도가 0 초과면 구축 불가능
                return false;
            UseCard(itemCard, () =>
            {
                selectedModule.SetHitpoints(selectedModule.maxHitpoints / 2);
                parentShip.UsePower(powerUsage);
            });
            return true;

        }
    }

    private bool Overheat(UIItemCard itemCard, HullDataRow hullData)
    {
        var powerUsage = parentShip.isEngineDown ? 1 : hullData.powerUsage;
        bool canUse = parentShip.power - powerUsage >= 0;
        if (!canUse)
            return false;
        if (isCardAboutToUse)
        {
            UseCard(itemCard, () =>
            {
                parentShip.SetOverHeat(true);
                parentShip.UsePower(powerUsage);
            });
            return true;
        }
        else
            return false;
    }

    private bool OperateDoomsday(UIItemCard itemCard, HullDataRow hullData)
    {
        var selectedModule = UIItemModule.selectedModule;
        if (selectedModule != null)
        {
            // 공격 대상이 내 모듈이면 아무 것도 하지 않는다.
            if (parentShip.isPlayer && selectedModule.isMyModule)
                return false;
            else if (selectedModule.hitpoints <= 0)
                return false;
            else
            {
                var powerUsage = hullData.powerUsage;
                var damage = 20;
                bool canUse = parentShip.power - powerUsage >= 0;

                if (canUse)
                {
                    UseCard(itemCard, () =>
                    {
                        parentShip.UsePower(powerUsage);

                        var startPos = doomsdayLauncherTransform.position;
                        var endPos = selectedModule.modulePosition;

                        doomsdayController.LaunchDoomsday(startPos, endPos, new Color32(0xA8, 0xFF, 0x00, 0xFF), new Color32(0xFF, 0xFF, 0xFF, 0xFF),
                            () =>
                            {
                                selectedModule.TakeDamage(damage);
                                CameraShaker.instance.ShakeCamera(2f, 110);
                            });
                    });
                }
                else
                    return false;

            }
            return true;
        }
        else
        {
            return false;
        }
    }
}
