﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MissileController : MonoBehaviour
{
    [SerializeField] List<Missile> missiles;

    int m_index;
    int maxIndex => missiles.Count;

    public void LaunchMissile(Vector2 startPos, Vector2 target, float rot, Action callback)
    {
        var missile = missiles[m_index];
            missile.SetVelocity(1.5f)
                   .SetStartPos(startPos)
                   .SetTarget(target)
                   .SetCallback(callback)
                   .SetStartRot(rot)
                   .MoveToTarget();
        m_index++;
        if (m_index >= maxIndex)
            m_index = 0;

    }
}
