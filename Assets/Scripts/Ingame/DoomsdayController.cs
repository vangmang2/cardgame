﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DoomsdayController : MonoBehaviour
{
    [SerializeField] Doomsday doomsday;

    Coroutine coroutine;
    public void LaunchDoomsday(Vector2 startPos, Vector2 endPos, Color32 startColor, Color32 endColor, Action launchCallback)
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoLaunchDoomsday(startPos, endPos, startColor, endColor, launchCallback));
    }

    private IEnumerator CoLaunchDoomsday(Vector2 startPos, Vector2 endPos, Color32 startColor, Color32 endColor, Action launchCallback)
    {
        yield return new WaitForSeconds(0.2f);
        doomsday.PlayAudioFire();
        launchCallback?.Invoke();
        doomsday.SetPositions(startPos, endPos)
                .SetColors(startColor, endColor)
                .PlayHitParticle(endPos);
        yield return new WaitForSeconds(2f);
        doomsday.SetPositions(Vector2.zero, Vector2.zero)
                .StopHitParticle();
    }
}
