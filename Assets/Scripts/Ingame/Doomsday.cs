﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doomsday : MonoBehaviour
{
    [SerializeField] LineRenderer lineRenderer;
    [SerializeField] ParticleSystem hitParticle;
    [SerializeField] AudioSource audioFire;

    public void PlayAudioFire()
    {
        audioFire.Play();
    }

    public Doomsday SetPositions(Vector2 startPos, Vector2 targetPos)
    {
        lineRenderer.SetPosition(0, startPos);
        lineRenderer.SetPosition(1, targetPos);
        return this;
    }

    public Doomsday SetColors(Color32 startColor, Color32 endColor)
    {
        lineRenderer.startColor = startColor;
        lineRenderer.endColor = endColor;
        return this;
    }

    public void PlayHitParticle(Vector2 targetPos)
    {
        hitParticle.transform.position = targetPos;
        hitParticle.Play();
    }

    public void StopHitParticle()
    {
        hitParticle.Stop();
    }
}
