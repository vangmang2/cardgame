﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Projectile : MonoBehaviour
{
    [SerializeField] Transform trBody;
    [SerializeField] SpriteRenderer sprProjectile;
    [SerializeField] TrailRenderer trailRenderer;
    [SerializeField] ParticleSystem hitParticle;
    [SerializeField] AudioSource audioExplosion, audioFired;

    float m_Velocity;
    Vector2 target;
    Action callback;

    public Projectile SetVelocity(float velocity)
    {
        m_Velocity = velocity;
        return this;
    }

    public Projectile SetImage(Sprite sprite)
    {
        sprProjectile.sprite = sprite;
        return this;
    }

    public Projectile SetStartPos(Vector2 target)
    {
        trBody.position = target;
        return this;
    }

    public Projectile SetTarget(Vector2 target)
    {
        this.target = target;
        return this;
    }

    public Projectile SetCallback(Action callback)
    {
        this.callback = callback;
        return this;
    }

    public Projectile SetTrailRendererColor(Color startColor, Color endColor)
    {
        trailRenderer.startColor = startColor;
        trailRenderer.endColor = endColor;
        return this;
    }

    public void MoveToTarget()
    {
        trBody.gameObject.SetActive(true);
        StartCoroutine(CoMoveToTarget());
    }

    private IEnumerator CoMoveToTarget()
    {
        audioFired.Play();

        Vector2 startPos = trBody.position;
        Vector2 endPos = target;

        var dir = (endPos - startPos);
        var rot = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90f;

        trBody.rotation = Quaternion.Euler(0f, 0f, rot);

        float t = 0f;
        while(t <= 1f)
        {
            t += m_Velocity * Time.deltaTime;
            trBody.position = Vector3.Lerp(startPos, endPos, t);
            yield return null;
        }
        trBody.gameObject.SetActive(false);
        callback?.Invoke();
        hitParticle.transform.position = trBody.position;
        hitParticle.Play();
        audioExplosion.Play();
    }
}
