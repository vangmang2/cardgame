﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ProjectileController : MonoBehaviour
{
    [SerializeField] List<Projectile> projectiles;

    int m_index;
    int maxIndex => projectiles.Count;

    public void FireProjectile(Vector2 startPos, Vector2 endPos, Sprite sprite, Color startColor, Color endColor, Action callback)
    {
        var projectile = projectiles[m_index];
        projectile.SetVelocity(5.5f)
                  .SetStartPos(startPos)
                  .SetTarget(endPos)
                  .SetImage(sprite)
                  .SetTrailRendererColor(startColor, endColor)
                  .SetCallback(callback)
                  .MoveToTarget();
        m_index++;
        if (m_index >= maxIndex)
            m_index = 0;

    }
}
