﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Missile : MonoBehaviour
{
    [SerializeField] Transform trBody;
    [SerializeField] ParticleSystem hitParticle;
    [SerializeField] AudioSource audioExplosion, audioMissileLaunch;

    float m_Velocity;
    Vector2 target;
    Action callback;

    public Missile SetVelocity(float velocity)
    {
        m_Velocity = velocity;
        return this;
    }

    public Missile SetStartPos(Vector2 pos)
    {
        trBody.position = pos;
        return this;
    }

    public Missile SetStartRot(float rot)
    {
        trBody.eulerAngles = new Vector3(0f, 0f, rot);
        return this;
    }

    public Missile SetTarget(Vector2 target)
    {
        this.target = target;
        return this;
    }

    public Missile SetCallback(Action callback)
    {
        this.callback = callback;
        return this;
    }

    public void MoveToTarget()
    {
        trBody.gameObject.SetActive(true);
        StartCoroutine(CoMoveToTarget());
    }

    private IEnumerator CoMoveToTarget()
    {
        audioMissileLaunch.Play();
        Vector3 startPos = trBody.position;
        Vector3 endPos = target;

        var dir = (endPos - startPos);
        var rot = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90f;

        Vector3 startRot = trBody.eulerAngles;
        Vector3 endRot = new Vector3(0f, 0f, rot);

        float t = 0f;
        while(t < 1f)
        {
            t += m_Velocity * Time.deltaTime;

            var center = (startPos + endPos) * 0.01f;
            //center -= new Vector3(0f, 5f, 0f);
            var _startPos = startPos - center;
            var _endPos = endPos - center;
            trBody.position = Vector3.Slerp(_startPos, _endPos, t);
            //trBody.eulerAngles = Vector3.Lerp(trBody.eulerAngles, endRot, t);
            yield return null;
        }

        //while((endPos - trBody.position).magnitude > 2f)
        //{
        //    trBody.position += trBody.up * m_Velocity * Time.deltaTime;
        //    
        //    yield return null;
        //}
        callback?.Invoke();
        hitParticle.transform.position = trBody.position;
        hitParticle.Play();
        audioExplosion.Play();
        yield return new WaitForSeconds(0.1f);
        trBody.gameObject.SetActive(false);
    }
}
