﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FxShield : MonoBehaviour
{
    [SerializeField] SpriteRenderer sprShield;
    [SerializeField] ParticleSystem particleShieldBroken;

    float duration = 0.35f;
    Ease ease = Ease.OutCubic;

    public void ActivateShield()
    {
        var color = new Color32(0x00, 0xFF, 0xEE, 0xFF);
        sprShield.color = color;

        sprShield.transform.DOKill();
        sprShield.transform.localScale = Vector3.one;
        sprShield.transform.DOScaleX(1.53f, duration).SetEase(ease);
        color.a = 0x00;
        sprShield.DOColor(color, duration).SetEase(ease);
    }

    public void PlayBrokenShieldParticle()
    {
        particleShieldBroken.Play();
    }
}
