﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShipPlayer : ShipBase
{
    // Start is called before the first frame update
    void Awake()
    {
        InitShip();
        maxPower = 2;
        power = maxPower;
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    public override void InvokeDeadEvent()
    {
        GameManager.instance.raycaster.enabled = true;
        GameManager.instance.goBtnRestart.SetActive(true);
    }

    public void ShowCards(Action callback)
    {
        cardDeck.ShowCards(callback);
    }

    public void ClearCards()
    {
        cardDeck.ClearCards();
    }
}
