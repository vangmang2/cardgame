﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using DG.Tweening;
using System.Reflection;

public class ShipEnemy : ShipBase
{
    readonly Queue<UIItemCard> cardQueue = new Queue<UIItemCard>();
    readonly Queue<Func<bool>> actionQueue = new Queue<Func<bool>>();
    readonly Queue<int> powerQueue = new Queue<int>();

    // Start is called before the first frame update
    void Awake()
    {
        InitShip();
    }

    public override void InvokeDeadEvent()
    {
        GameManager.instance.raycaster.enabled = true;
        GameManager.instance.goClear.SetActive(true);
        GameManager.instance.goBtnRestart.SetActive(true);
    }

    Coroutine coroutine;
    public void PlayAI()
    {
        moduleWeapon.EnableCardUsable();
        moduleShield.EnableCardUsable();
        moduleEngine.EnableCardUsable();
        moduleHull.EnableCardUsable();

        UpdateInfo();
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoPlayAI());
    }

    private IEnumerator CoPlayAI()
    {
        yield return new WaitForSeconds(2f);
        EvaulatePriority();
        // AI 로직
        // 공격 우선순위 -> 쉴드 -> 무기 -> 엔진 -> 선체
        // 공격 우선순위 -> 우라늄 탄환
        
        while (actionQueue.Count > 0 && power > 0)
        {
            if (UIItemModule.selectedModule != null && UIItemModule.selectedModule.hitpoints <= 0)
            {
                var rand = new System.Random();
                var moduleList = targetShip.moduleList.Where(module => module.hitpoints > 0).ToList();
                var randomModule = moduleList[rand.Next(0, moduleList.Count)];
                UIItemModule.selectedModule = randomModule;
            }

            var card = cardQueue.Dequeue();
            var action = actionQueue.Dequeue();
            DrawCard(card, action);
            yield return new WaitForSeconds(3f);
        }
        GameManager.instance.turnIndex--;
        GameManager.instance.ToMyTurn();
        cardDeck.ClearCards();
        cardQueue.Clear();
        actionQueue.Clear();
        powerQueue.Clear();
    }

    private void EvaulatePriority()
    {        
        // 엔진 부터 구축
        if(moduleEngine.hitpoints <= 0)
        {
            Debug.Log("엔진 재구축");
            var moduleRebuild = cardDeck.cardList.FirstOrDefault(card => card.cardName == "모듈 재구축");
            if(moduleRebuild != null)
            {
                UIItemModule.emenyTempModule = moduleEngine;
                actionQueue.Enqueue(moduleRebuild.UseCard);
                cardQueue.Enqueue(moduleRebuild);
                powerQueue.Enqueue(1);
            }
        }
        else
        {
            // 둠스데이
            var doomsday = cardDeck.cardList.FirstOrDefault(card => card.cardName == "둠스데이");
            if (doomsday != null)
            {
                Debug.Log("둠스데이");
                var rand = new System.Random();
                var moduleList = targetShip.moduleList.Where(module => module.hitpoints > 0).ToList();
                var randomModule = moduleList[rand.Next(0, moduleList.Count)];
                UIItemModule.selectedModule = randomModule;

                actionQueue.Enqueue(doomsday.UseCard);
                cardQueue.Enqueue(doomsday);
                powerQueue.Enqueue(moduleEngine.hitpoints > 0 ? 5 : 1);
            }
        }

        // 그다음 우라늄 탄
        if (moduleWeapon.hitpoints <= 0)
        {
            var moduleRebuild = cardDeck.cardList.FirstOrDefault(card => card.cardName == "모듈 재구축");
            if (moduleRebuild != null)
            {
                Debug.Log("무기 고치기");
                UIItemModule.emenyTempModule = moduleWeapon;
                actionQueue.Enqueue(moduleRebuild.UseCard);
                cardQueue.Enqueue(moduleRebuild);
                powerQueue.Enqueue(1);
            }
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                // 차례대로 우라늄, 미사일, 플라즈마
                var uranium = cardDeck.cardList.FirstOrDefault(cardDeck => cardDeck.cardName == "우라늄 포탄");
                int powerUsage = moduleEngine.hitpoints > 0 ? 3 : 1;
                bool canUse = (maxPower - powerQueue.Sum()) - powerUsage >= 0;
                if (!canUse)
                    break;

                if (uranium != null)
                {
                    Debug.Log("우라늄");
                    var rand = new System.Random();
                    var moduleList = targetShip.moduleList.Where(module => module.hitpoints > 0).ToList();
                    var randomModule = moduleList[rand.Next(0, moduleList.Count)];
                    UIItemModule.selectedModule = randomModule;

                    actionQueue.Enqueue(uranium.UseCard);
                    cardQueue.Enqueue(uranium);
                    powerQueue.Enqueue(powerUsage);
                }
            }

            for (int i = 0; i < 2; i++)
            {
                var missile = cardDeck.cardList.FirstOrDefault(cardDeck => cardDeck.cardName == "미사일");
                int powerUsage = moduleEngine.hitpoints > 0 ? 2 : 1;
                bool canUse = (maxPower - powerQueue.Sum()) - powerUsage >= 0;
                if (!canUse)
                    break;

                if (missile != null)
                {
                    Debug.Log("미사일");
                    actionQueue.Enqueue(missile.UseCard);
                    cardQueue.Enqueue(missile);
                    powerQueue.Enqueue(powerUsage);
                }
            }

            for (int i = 0; i < 2; i++)
            {
                var plasma = cardDeck.cardList.FirstOrDefault(cardDeck => cardDeck.cardName == "플라즈마 포탄");
                int powerUsage = 1;
                bool canUse = (maxPower - powerQueue.Sum()) - powerUsage >= 0;
                if (!canUse)
                    break;

                if (plasma != null)
                {

                    Debug.Log("플라즈마");
                    var rand = new System.Random();
                    var moduleList = targetShip.moduleList.Where(module => module.hitpoints > 0).ToList();
                    var randomModule = moduleList[rand.Next(0, moduleList.Count)];
                    UIItemModule.selectedModule = randomModule;

                    actionQueue.Enqueue(plasma.UseCard);
                    cardQueue.Enqueue(plasma);
                    powerQueue.Enqueue(powerUsage);
                }
            }
        }
    }

    private void DrawCard(UIItemCard itemCard, Func<bool> callback)
    {
        itemCard.getRtBody.gameObject.SetActive(true);
        itemCard.getRtBody.localScale = Vector3.zero;
        itemCard.getRtBody.anchoredPosition = new Vector3(-727f, -442f);
        itemCard.getRtBody.rotation = Quaternion.Euler(0f, 0f, -117.2f);

        var endPos = new Vector3(-188f, -619f);
        var endRot = Vector3.zero;

        itemCard.getRtBody.DOAnchorPos(endPos, 0.65f).SetEase(Ease.InOutSine);
        itemCard.getRtBody.DOScale(Vector3.one * 1.5f, 0.65f).SetEase(Ease.InOutSine);
        itemCard.getRtBody.DORotate(endRot, 0.65f).SetEase(Ease.InOutSine).OnComplete(() =>
        {
            itemCard.getRtBody.DOAnchorPosY(endPos.y + 10f, 1.2f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                callback?.Invoke();
            });
        });
    }
}
