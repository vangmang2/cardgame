﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipBase : MonoBehaviour
{
    public bool isPlayer;

    [SerializeField] protected ModuleWeapon moduleWeapon;
    [SerializeField] protected ModuleShield moduleShield;
    [SerializeField] protected ModuleEngine moduleEngine;
    [SerializeField] protected ModuleHull moduleHull;

    [SerializeField] GameObject goShieldBattery, goShip, goUIModules;
    [SerializeField] ParticleSystem explosionParticle;

    [SerializeField] protected ShipBase targetShip;

    public ShipBase getTargetShip => targetShip;

    public List<ModuleBase> moduleList { get; private set; } 

    public HUDShield hudShield;
    public HUDPower hudPower;
    public FxShield fxShield;
    public UICardDeck cardDeck;

    public readonly int maxLimitPower = 10;
    public int maxHitpoints { get; private set; }
    public int maxPower { get; protected set; }
    public int maxShield { get; private set; }

    public int hitpoints;
    public int power;
    public int currentShield;
    public int shieldHitpoints => moduleShield.hitpoints;

    public bool isShieldBatteryEnable { get; private set; }
    public int shieldBatteryDuration { get; private set; }
    public bool isOverload { get; private set; } // 과부하
    public int overheatDuration { get; private set; }
    public bool isOverHeat { get; private set; } // 과열
    public int powerBatteryCount;
    public bool isPowerBatteryEnable => powerBatteryCount > 0;
    public bool isEngineDown => moduleEngine.hitpoints <= 0;

    public void RecoveryShield(int amount)
    {
        moduleShield.GainHitpoints(amount);
        //shield = Mathf.Min(shield, maxShield);
        //hudShield.SetShield(shield, maxShield);
    }

    public void RecoveryPower(int amount)
    {
        power += amount;
        power = Mathf.Min(power, maxPower);
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    public void TakeDamage(int amount)
    {
        hitpoints -= amount;
        hitpoints = Mathf.Max(hitpoints, 0);
        if(hitpoints <= 0)
        {
            //DEAD
            goShip.SetActive(false);
            goUIModules.SetActive(false);
            explosionParticle.Play();
            InvokeDeadEvent();
        }
    }

    public virtual void InvokeDeadEvent()
    {

    }

    public int ReduceShield(int amount)
    {
        int overTakenDamage = currentShield - amount;
        currentShield -= amount;
        currentShield = Mathf.Max(currentShield, 0);
        hudShield.SetShield(currentShield, maxShield);
        if (currentShield > 0)
            fxShield.ActivateShield();
        else
            fxShield.PlayBrokenShieldParticle();
        return overTakenDamage;
    }

    public void UsePower(int usageAmount)
    {
        power -= usageAmount;
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    public void InitShip()
    {
        moduleList = new List<ModuleBase>();
        moduleList.Add(moduleWeapon);
        moduleList.Add(moduleShield);
        moduleList.Add(moduleEngine);
        moduleList.Add(moduleHull);

        moduleWeapon.SetMaxHitpoints(50);
        moduleShield.SetMaxHitpoints(12);
        moduleEngine.SetMaxHitpoints(45);
        moduleHull.SetMaxHitpoints(75);

        moduleWeapon.SetInfo();
        moduleShield.SetInfo();
        moduleEngine.SetInfo();
        moduleHull.SetInfo();

        maxHitpoints = 75;
        hitpoints = maxHitpoints;
        maxPower = 3;
        power = maxPower;
        maxShield = 12;
        currentShield = maxShield;

        hudShield.SetShield(currentShield, maxShield);
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    public void ActivateShieldBattery(bool enable)
    {
        isShieldBatteryEnable = enable;
        if (enable)
        {
            moduleShield.PlayShieldBatteryEffect();
            shieldBatteryDuration = 3;
        }
        else
        {
            moduleShield.StopShieldBatteryEffect();
        }
        goShieldBattery.SetActive(enable);
    }

    public void ReduceShieldBatteryDuration()
    {
        shieldBatteryDuration--;
        shieldBatteryDuration = Mathf.Max(shieldBatteryDuration, 0);
    }

    public void ReduceOverheatDuration()
    {
        overheatDuration--;
        overheatDuration = Mathf.Max(overheatDuration, 0);
    }

    public void SetOverload(bool enable)
    {
        isOverload = enable;
    }

    public void SetOverHeat(bool enable)
    {
        isOverHeat = enable;
        if (enable)
        {
            overheatDuration = 2;
            moduleWeapon.PlayOverheatParticle();
        }
        else
            moduleWeapon.StopOverheatParticle();
    }

    public void EnablePowerBattery(bool enable)
    {
        if (enable)
        {
            powerBatteryCount = 2;
            cardDeck.cardList.ForEach((card) =>
            {
                if(card.moduleType != ModuleType.Hull)
                    card.SetPowerUsageText(1);
            });
        }
        else
        {
            cardDeck.cardList.ForEach((card) =>
            {
                card.ResetPowerUsageText();
            });
        }
    }

    // 턴이 종료되고 정보 업데이트
    public virtual void UpdateInfo()
    {
        UpdateEngine();
        UpdateShield();
        UpdateWeapon();

        moduleWeapon.RegisterCard();
        moduleShield.RegisterCard();
        moduleEngine.RegisterCard();
        moduleHull.RegisterCard();
    }

    protected void UpdateEngine()
    {
        if (moduleEngine.hitpoints > 0)
        {
            maxPower++;
            maxPower = Mathf.Min(maxLimitPower, maxPower);
            power = maxPower;
            if(isOverload)
            {
                power /= 2;
                SetOverload(false);
            }
        }
        else
        {
            // 엔진 모듈 내구도 0되면 카드 사용량 전부 1 됨.
            EnablePowerBattery(true);
            powerBatteryCount = 3;
            maxPower = 2;
            power = maxPower;
        }
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    protected void UpdateShield()
    {
        currentShield = moduleShield.hitpoints;
        hudShield.SetShield(currentShield, maxShield);
        ReduceShieldBatteryDuration();
        if (shieldBatteryDuration == 0)
            ActivateShieldBattery(false);
    }

    protected void UpdateWeapon()
    {
        ReduceOverheatDuration();

        if (overheatDuration == 0)
        {
            SetOverHeat(false);
        }
    }

    protected void UpdateHull()
    {

    }
}
