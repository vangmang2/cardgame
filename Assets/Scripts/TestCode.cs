﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCode : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach(var weapon in DataManager.weaponData.dataList)
        {
            Debug.Log($"id:{weapon.id} name:{weapon.name} description:{weapon.description} damage:{weapon.damage} powerUsage:{weapon.powerUsage}");
        }

        Debug.Log(DataManager.weaponData.Get(1));

        Debug.Log("--------------------------------");

        foreach (var shield in DataManager.shieldData.dataList)
        {
            Debug.Log($"id:{shield.id} name:{shield.name} description:{shield.description} powerUsage{shield.powerUsage}");
        }
        Debug.Log("--------------------------------");

        foreach (var engine in DataManager.engineData.dataList)
        {
            Debug.Log($"id:{engine.id} name:{engine.name} description:{engine.description} powerUsage{engine.powerUsage}");
        }
        Debug.Log("--------------------------------");

        foreach (var hull in DataManager.hullData.dataList)
        {
            Debug.Log($"id:{hull.id} name:{hull.name} description:{hull.description} powerUsage{hull.powerUsage}");
        }
    }

}
