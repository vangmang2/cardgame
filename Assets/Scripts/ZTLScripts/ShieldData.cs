// ZFExcelExporter에서 생성된 코드입니다.

using System.Collections.Generic;
using ZeroFormatter;


namespace ZF
{
    [ZeroFormattable]
    public partial class ShieldDataRow : IRow
    {

        [Index(0)] public virtual int id { get; set; }

        [Index(1)] public virtual string name { get; set; }

        [Index(2)] public virtual string description { get; set; }

        [Index(3)] public virtual int powerUsage { get; set; }

        [Index(4)] public virtual int cooldown { get; set; }

        [Index(5)] public virtual int[] dummy { get; set; }

    }
    public partial class ShieldDataTable : Table<int, ShieldDataRow>
    {
        public ShieldDataTable(Table<int, ShieldDataRow> table)
        {
            indexedData = table.indexedData;
            dataList = table.dataList;
        }
        
        [UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void __initRegister()
        {
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, Table<int, ShieldDataRow>>.Register(new TableFormatter<ZeroFormatter.Formatters.DefaultResolver, int, ShieldDataRow>());
            ZeroFormatter.Formatters.Formatter.RegisterDictionary<ZeroFormatter.Formatters.DefaultResolver, int, ShieldDataRow>();
        }

        private static string __encryptKey = @"";
        public static ShieldDataTable LoadBinary()
        {
            // load binary
            byte[] textBytes = UnityEngine.Resources.Load<UnityEngine.TextAsset>(@"TableBinary\ShieldData").bytes;
            if (__encryptKey.Length != 0)
            {
                var cryptoBytes = System.Text.Encoding.UTF8.GetBytes(__encryptKey);
                for (int i = 0; i < textBytes.Length; i++)
                    textBytes[i] ^= cryptoBytes[i & (cryptoBytes.Length - 1)];
            }
            var table = new ShieldDataTable(ZeroFormatterSerializer.Deserialize<Table<int, ShieldDataRow>>(textBytes))
            {
                dataList = new List<ShieldDataRow>()
            };
            foreach (var pair in table.indexedData)
                table.dataList.Add(pair.Value);
            return table;
        }
    }
}