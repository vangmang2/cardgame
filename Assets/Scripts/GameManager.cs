﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; private set; }

    public int turnIndex;

    public ShipPlayer playerShip;
    public ShipEnemy enemyShip;

    public RectTransform rtPlayerDeck;
    public GraphicRaycaster raycaster;

    public GameObject goBtnRestart, goClear;

    [SerializeField] UIItemExitBtn itemExitBtn;


    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    void Awake()
    {
        turnIndex = 1;
        instance = this;
    }

    void Start()
    {
        ToMyTurn();
    }

    public bool isMyTurn;

    public void ExitTurn()
    {
        raycaster.enabled = false;
        isMyTurn = false;
        itemExitBtn.SetDim(true);
        playerShip.ClearCards();
        turnIndex++;
        enemyShip.PlayAI();
    }

    public void ToMyTurn()
    {
        raycaster.enabled = true;
        turnIndex++;
        playerShip.cardDeck.ClearCards();
        playerShip.UpdateInfo();
        playerShip.ShowCards(() =>
        {
            isMyTurn = true;
            itemExitBtn.SetDim(false);
        });
    }

    public bool CooldownChecker(int cooldown)
    {
        //Debug.Log(getTurnIndex + " " + cooldown + " " + getTurnIndex % cooldown);
        bool check = cooldown == 1 || turnIndex % cooldown == 0;
        return check;
    }

    public void OnClick_ExitTurn()
    {
        ExitTurn();
    }
}
