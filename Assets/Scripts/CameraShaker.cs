﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Camera))]
public class CameraShaker : MonoBehaviour
{
    public static CameraShaker instance { get; private set; }


    public bool isShaking;

    Vector2 randomTarget;
    float t;
    bool opposite;

    void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (!isShaking)
        {
            t += Time.deltaTime;
            if(t >= 1f)
            {
                randomTarget = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * 0.04f;
                randomTarget *= opposite ? 1f : -1f;
                opposite = !opposite;
                transform.DOMove(randomTarget, 1f).SetEase(Ease.InOutSine);
                t = 0f;
            }
        }
    }

    [Button("Shake")]
    public void ShakeCamera(float duration = 0.4f, int vibrato = 30)
    {
        isShaking = true;
        transform.DOShakePosition(duration, 0.25f, vibrato).OnComplete(() =>
        {
            isShaking = false;
        });
    }
}
